# cloudvm_maker

This role allows you to quickly spin up instances on KVM locally utilizing
cloud images.

This works via attaching an iso image to the running instance to provide
necessary items such as hostname and ssh keys.

Many thanks to Chris for the instructions:

https://blog.christophersmart.com/2016/06/17/booting-fedora-24-cloud-image-with-kvm/

Instructions for making DNS resolving work: https://liquidat.wordpress.com/2017/03/03/howto-automated-dns-resolution-for-kvmlibvirt-guests-with-a-local-domain


```sh
virt-install --connect qemu:///system --import --name atomic1 --vcpus 4 --ram 8192 \
  --disk path=$HOME/images/fa1.qcow2,bus=virtio,format=qcow2 \
  --serial pty --cpu host --rng=/dev/random \
  --network=network:default,model=virtio --noautoconsole \
  --disk path=$HOME/images/config.iso,device=cdrom
```
